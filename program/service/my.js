import http from '../uilts/http'

//我的 获取验证码
export const requestCode=(params)=>http.post('/p/sms/send',params)
//我的 确定按钮
export const requestSure=(params)=>http.put('/user/registerOrBindUser',params)
//我的 查看全部 
export const requestAll =(params)=>http.get('/p/myOrder/myOrder',params)
//我的 取消订单
export const requestnoOrder=(params)=>http.put('/p/myOrder/cancel/'+params)
//我的 删除订单
export const requestDel=(params)=>http.delete('/p/myOrder/'+params)
//我的 获取次数
export const requestunPay=()=>http.get('/p/myOrder/orderCount')
//我的 收藏次数
export const requestCount =()=>http.get('/p/user/collection/count')
//我的 收藏数据
export const requestData=(params)=>http.get('/p/user/collection/prods',params)
//我的 收货地址
export const requestAddress=()=>http.get('/p/address/list')
//我的 修改地址
export const requestgetAddress=(params)=>http.put('/p/address/defaultAddr/'+params,{addrId:params} )
// //我的 编辑收货地址 
export const requestEditAddress=(params)=>http.get('/p/address/addrInfo/'+params)
//我的 保存收货地址
export const requestShipToAddress=(params)=>http.put('/p/address/updateAddr',params)
//我的 删除收货地址
export const requestDelete=(params)=>http.delete('/p/address/deleteAddr/'+params)
//我的 保存收货地址
export const requestAddAddress=(params)=>http.post('/p/address/addAddr',params)
//我的 跳转详情页
export const requestJumpD=(params)=>http.get('/p/myOrder/orderDetail',params)
//获取授权请求头像昵称
export const requestsetUserInfo=(params)=>http.put('/p/user/setUserInfo',params)