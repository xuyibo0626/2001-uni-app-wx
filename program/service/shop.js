import http from '../uilts/http'

//购物车数据
export const shopList=()=>http.post('/p/shopCart/info');
//购物车选择数据
export const shopTotal=(data)=>http.post('/p/shopCart/totalPay',data);
//购物车总数量
export const shopCountData=()=>http.get('/p/shopCart/prodCount');
//购物车改变数据
export const shopChangeItemData=(data)=>http.post('/p/shopCart/changeItem',data);
//删除购物车数据
export const shopdeleteItemData=(data)=>http.delete('/p/shopCart/deleteItem',data);
//测试数据
export const ProdTag = (params) => http.get("/prod/tag/prodTagList", params);



